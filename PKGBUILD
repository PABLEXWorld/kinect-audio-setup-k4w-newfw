# Maintainer: Mikołaj "D1SoveR" Banasik <d1sover@gmail.com>
# Maintainer: 3l H4ck3r C0mf0r7 <pablexworld13@gmail.com>
pkgname="kinect-audio-setup-k4w-newfw"
pkgver=0.5
pkgrel=4
pkgdesc='Tools to download and apply USB Audio Class firmware for Kinect and use it as microphone'
arch=('x86_64' 'i686')
url='https://git.ao2.it/kinect-audio-setup.git'
license=('WTFPL' 'BSD')
depends=('libusb' 'systemd-libs')
makedepends=('make' 'gcc' 'msitools' 'fuse2' 'cabextract')
provides=('kinect-audio-setup')
conflicts=('kinect-audio-setup')
_pkg='kinect-audio-setup'

source=("git+${url}#tag=v${pkgver}"
        "git+https://github.com/jmattsson/fuseloop.git"
        'https://download.microsoft.com/download/E/C/5/EC50686B-82F4-4DBF-A922-980183B214E6/KinectRuntime-v1.8-Setup.exe'
        'LICENSE'
        'Kinect-for-Windows.patch')
sha256sums=('SKIP'
            'SKIP'
            'f4d4143fb0f0a8d276889c077bfc8af42bfe99c128cadab5e316bf015a9858e9'
            'd23efd383bc03aa8cdeac33be24a9c915f05ad92d20f4070e7160bdcff7f4a8c'
            '874ceb3fc1cc1ea5cbb41dcfccf04e4d283838a36e62f04909433b99329faf6b')

UPLOADER_PATH='/usr/bin/kinect_upload_fw'
FIRMWARE_PATH='/usr/lib/firmware/kinect_uac_firmware.bin'

prepare() {
  msg2 "$(gettext "Applying Kinect for Windows fixes...")"
  patch -d kinect-audio-setup -p1 < Kinect-for-Windows.patch
  msg2 "$(gettext "Extracting the firmware out of the Kinect for Windows Runtime package...")"
  make -C fuseloop
  touch AttachedContainer.cab
  fuseloop/fuseloop -O 622624 -S 115650048 -r KinectRuntime-v1.8-Setup.exe AttachedContainer.cab
  cabextract -F a2 AttachedContainer.cab
  msiextract a2 # KinectDrivers-v1.8-x86.WHQL.msi
  fusermount -u AttachedContainer.cab
  rm AttachedContainer.cab
  msg2 "$(gettext "Generating the udev rules file...")"
  cp "${srcdir}/${_pkg}/contrib/55-kinect_audio.rules.in" "${srcdir}/55-kinect-audio.rules"
  "${srcdir}/${_pkg}/kinect_patch_udev_rules" "$FIRMWARE_PATH" "$UPLOADER_PATH" "${srcdir}/55-kinect-audio.rules"
}

build() {
  cd "${_pkg}/kinect_upload_fw"
  make kinect_upload_fw
}

package() {
  install -Dm644 "${srcdir}/Program Files/Microsoft Kinect Drivers/Service/UAC.bin" "${pkgdir}${FIRMWARE_PATH}"
  install -Dm755 "${srcdir}/${_pkg}/kinect_upload_fw/kinect_upload_fw" "${pkgdir}${UPLOADER_PATH}"
  install -Dm644 "${srcdir}/55-kinect-audio.rules" "${pkgdir}/usr/lib/udev/rules.d/55-kinect-audio.rules"
  install -Dm644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${_pkg}/LICENSE"
}
